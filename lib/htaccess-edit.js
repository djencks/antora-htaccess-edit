'use strict'

const expandPath = require('@antora/expand-path-helper')
const { readFileSync, writeFileSync } = require('fs')

module.exports.register = (eventEmitter, config = {}) => {
  const {
    component, version,
    htaccessSource = '.htaccess',
    htaccessTarget = '.htaccess-x',
    htaccessNotFound = '.htaccess-nf',
    sliceAt = -1,
  } = config
  if (!component || !version) {
    console.log('component and version required for htaccess-edit-plugin')
    return
  }
  delete config.htaccess
  eventEmitter.on('afterClassifyContent', (playbook, contentCatalog) => {
    const htaccessSourcePath = expandPath(htaccessSource, '~+', playbook.dir)
    const htaccessTargetPath = expandPath(htaccessTarget, '~+', playbook.dir)
    const htaccessNotFoundPath = expandPath(htaccessNotFound, '~+', playbook.dir)
    const contents = readFileSync(htaccessSourcePath).toString()
    const oldLines = contents.split('\n')

    const newLines = contentCatalog.findBy(config).reduce((accum, page) => {
      if (page.out && page.out.path) {
        //test snippet
        const path = '/' + page.out.path
        const base = page.out.basename
        const re = new RegExp(` (/[^ ]*)[ ]+(/[^ ]*${base})$`)
        for (let i = 0; i < oldLines.length;) {
          const m = oldLines[i].match(re)
          if (m && path.endsWith(doSlice(m[2], sliceAt))) {
            accum.push(oldLines[i].replace(re, ` ${m[1]} ${path}`))
            oldLines.splice(i, 1)
          } else {
            i++
          }
        }
        //end test snippet
      }
      return accum
    }, []).sort()
    writeFileSync(htaccessTargetPath, newLines.join('\n'))
    writeFileSync(htaccessNotFoundPath, oldLines.join('\n'))
  })
}

function doSlice (target, at) {
  let segments = target.split('/').slice(1)
  segments = segments.slice(at)
  return '/' + segments.join('/')
}
