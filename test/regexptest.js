/* eslint-env mocha */
'use strict'

const { expect } = require('chai')

function doReplace (page, oldLines, sliceAt = 0) {
  const accum = []

  //code from lib
  const path = '/' + page.out.path
  const base = page.out.basename
  const re = new RegExp(` (/[^ ]*)[ ]+(/[^ ]*${base})$`)
  for (let i = 0; i < oldLines.length;) {
    const m = oldLines[i].match(re)
    if (m && path.endsWith(doSlice(m[2], sliceAt))) {
      accum.push(oldLines[i].replace(re, ` ${m[1]} ${path}`))
      oldLines.splice(i, 1)
    } else {
      i++
    }
  }
  //end code from lib
  return accum
}

function doSlice (target, at) {
  let segments = target.split('/').slice(1)
  segments = segments.slice(at)
  return '/' + segments.join('/')
}

describe('regexp test', () => {
  it('1', () => {
    const page = {
      src: { stem: 'application-dependencies' },
      out: { path: 'documentation/documentation/application-dependencies.html', basename: 'application-dependencies.html' },
    }
    const oldLines = ['redirect 301 /documentation/application-dependencies.html /documentation/application-dependencies.html']
    const accum = doReplace(page, oldLines)

    expect(accum[0]).to.equal('redirect 301 /documentation/application-dependencies.html /documentation/documentation/application-dependencies.html')
  })

  it('2', () => {
    const page = {
      src: { stem: 'application-dependencies' },
      out: { path: 'documentation/documentation/application-dependencies.html', basename: 'application-dependencies.html' },
    }
    const oldLines = ['redirect 301 /documentation/application-dependencies.html   /documentation/application-dependencies.html']
    const accum = doReplace(page, oldLines)

    expect(accum[0]).to.equal('redirect 301 /documentation/application-dependencies.html /documentation/documentation/application-dependencies.html')
  })

  it('slice', () => {
    expect(doSlice('/foo/bar/baz.adoc', -1)).to.equal('/baz.adoc')
    expect(doSlice('/foo/bar/baz.adoc', -2)).to.equal('/bar/baz.adoc')
    expect(doSlice('/foo/bar/baz.adoc', 1)).to.equal('/bar/baz.adoc')
    expect(doSlice('/foo/bar/baz.adoc', 0)).to.equal('/foo/bar/baz.adoc')
  })
})
